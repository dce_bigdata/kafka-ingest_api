# Data Ingestion to Kafka using REST API

### Things to consider
1. Use only alphanumeric or underscore ('\_') as a topic name
2. Run using this command:
```
go build --tags static
./kafka-ingest_api --topic="test4" --avro="avro-schema/test4.avsc"
```

