package main

import (
	"github.com/gin-gonic/gin"
	//"fmt"
	"flag"
	"net/http"
	. "../kafka-pubsub-go-confluent-linkedin/kafka-pubsub"
)

var router *gin.Engine

func RunProduce(c *gin.Context) {
    var data    interface{}
    topic := c.Param("topic")
    if err := c.ShouldBindJSON(&data); err == nil {
	//ProduceKafka(topic, data) // Produce data to Kafka topic
	ProduceKafkaAvro(topic, data) // Produce data to Kafka topic
        c.JSON(http.StatusOK, gin.H{"status": http.StatusOK})
    } else {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
    }
}

func main() {

  // Set the router as the default one provided by Gin
  router = gin.Default()

  // Initialize kafka
  //InitKafkaProducer()
  var topic,avroFile string
  flag.StringVar(&topic, "topic", "test","a string")
  flag.StringVar(&avroFile, "avro", "avroFile/test.avsc", "a string")
  flag.Parse()

  InitKafkaProducerAvro(topic, avroFile)

  // Initialize the routes
  router.POST("/:topic", RunProduce)

  // Start serving the application
  router.Run("0.0.0.0:8020")
}
